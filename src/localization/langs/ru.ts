export const ru = {
  pages: {
    home: {
      searchBox: "Что сегодня переводим?",
      searchTips: [
        "Select japanese letter",
        "Or several",
        "Or input search reqeust",
        "Or all at once",
        "Focus input box and press Enter key",
        "Or wait 300ms ＼(≧▽≦)／",
        "Clear search request for watch this awesome tips again ( ´ ∀ `)ノ～ ♡"
      ]
    },
    search: {
      columns: {
        japanese: "Japanese",
        romaji: "Romaji",
        sound: "Sound",
        explanation: "Explanation"
      }
    }
  }
};
