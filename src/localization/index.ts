import { en } from "./langs/en";
import { ru } from "./langs/ru";

export const messages = { en, ru };
