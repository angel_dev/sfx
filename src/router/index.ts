import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Search from "../views/Search.vue";
import Example from "../views/Example.vue";
import NotFound from "../views/NotFound.vue";

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    component: Home,
    children: [
      {
        path: "/",
        name: "home",
        component: Example
      },
      {
        path: "/search",
        name: "search",
        component: Search
      },
      {
        path: "/not-found",
        name: "not-found",
        component: NotFound
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
