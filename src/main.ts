import "normalize.css";

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

import VueI18n from "vue-i18n";
Vue.use(VueI18n);

import { messages } from "./localization";
const i18n = new VueI18n({ locale: "en", messages });

new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount("#app");
